var monographs;
var indexOfUpdatingMonograph;
var myActiveToken;



function readMyMonograph() {

  const Http = new XMLHttpRequest();
  const url='http://localhost:8080/monographs';
  Http.open("GET", url);
  
    Http.setRequestHeader("token", myActiveToken);
	
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	document.getElementById("show_mono").innerHTML = "";
	var obj = JSON.parse(Http.responseText);
	monographs = obj;
	
	for(i = 0; i < obj.length; i++){
	
	document.getElementById("show_mono").innerHTML += "<table><tr><td style=\"width: 75%\">" + obj[i].author.lastName + " " + obj[i].author.firstName + ", " + "<i>" + obj[i].title + "</i>" + ", " + "Wyd. " + obj[i].publishingHouse + ", " + obj[i].city + " " + obj[i].year + ". " + "</td><td>" +
	"<button id=\"delete_monograph\" onclick=\"deleteMonograph(" + obj[i].monographId + ")\"><img src=\"images/rubbish-bin.png\" height='32'></button>" +
	"<button id=\"edit_monograph\" onclick=\"editMonograph(" + i + ")\"><img src=\"images/edit.png\" height='32'></button>" + "</td></tr></table" +
	"<br>";

	}
	}
  }
}

function addMonograph(){
	document.getElementById("add_my_monograph").style.display = "block";
	
	var imgOne = document.createElement("img");
	imgOne.src = "images/photo2.jpg";

	var src = document.getElementById("add_photo");
	src.appendChild(imgOne);
	
	var imgTwo = document.createElement("img");
	imgTwo.src = "images/photo3.jpg";

	var src = document.getElementById("add_photo");
	src.appendChild(imgTwo);
	
}


function addAuthor(fn, ln){
	var author = new Object();
	author.firstName = document.getElementsByName(fn)[0].value;
	author.lastName = document.getElementsByName(ln)[0].value;
	var authorJson = JSON.stringify(author);
	console.log(authorJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/authors';
	Http.open("PUT", url);
	Http.setRequestHeader("token", myActiveToken);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(authorJson);
}


function selectAuthor(c){
	var x = document.getElementById(c);
	
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/authors';
	Http.open("GET", url);
	Http.setRequestHeader("token", myActiveToken);
	Http.send();
	Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	var obj = JSON.parse(Http.responseText);
	
	for(i = 0; i < obj.length; i++){
		var option = document.createElement("option");
		option.text += obj[i].lastName + " " + obj[i].firstName;	
		option.value = JSON.stringify(obj[i]);
		x.add(option);
   
	}
	}
  }
}


function submitMonograph(){
	var monograph = new Object();
	monograph.author = document.getElementById("author_combobox").value;
	monograph.title = document.getElementsByName("mono_title")[0].value;
	monograph.publishingHouse = document.getElementsByName("mono_publishing")[0].value;
	monograph.city = document.getElementsByName("mono_city")[0].value;
	monograph.year = document.getElementsByName("mono_year")[0].value;
	var monographJson = JSON.stringify(monograph);
	console.log(monographJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/monographs';
	Http.open("PUT", url);
	Http.setRequestHeader("token", myActiveToken);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(monographJson);
	
	document.getElementById("add_my_monograph").style.display = "none";
	
	var src = document.getElementById("add_photo");
    src.parentNode.removeChild(src);
	
	alert('Udało Ci się dodać książkę!:) Aby zobaczyć, ponownie wczytaj książki:)');
}

function deleteMonograph(monographId)
{
 const Http = new XMLHttpRequest();
  const url='http://localhost:8080/monographs/'+monographId;
  Http.open("DELETE", url);
  Http.setRequestHeader("token", myActiveToken);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)	
	}
	}
	if (confirm('Udało Ci się usunąć książkę!:)')) {
     alert('Aby zobaczyć, ponownie wczytaj książki:)');
	} 
  }
  
function editMonograph(index){
	document.getElementById("edit_my_monograph").style.display = "block";
	indexOfUpdatingMonograph = index;
	document.getElementsByName("update_f_name")[0].value = monographs[index].author.firstName;
	document.getElementsByName("update_l_name")[0].value = monographs[index].author.lastName;	
	document.getElementsByName("update_mono_title")[0].value = monographs[index].title;
	document.getElementsByName("update_mono_publishing")[0].value = monographs[index].publishingHouse;
	document.getElementsByName("update_mono_city")[0].value = monographs[index].city;
	document.getElementsByName("update_mono_year")[0].value = monographs[index].year;
}

function updateMonograph()
{
	
	var monograph = new Object();
	monograph.author = new Object();
	monograph.monographId = monographs[indexOfUpdatingMonograph].monographId;
	
	monograph.author = document.getElementById("update_combobox_authors").value;
	monograph.title = document.getElementsByName("update_mono_title")[0].value;
	monograph.publishingHouse = document.getElementsByName("update_mono_publishing")[0].value;
	monograph.city = document.getElementsByName("update_mono_city")[0].value;
	monograph.year = document.getElementsByName("update_mono_year")[0].value;
	
	var monographJson = JSON.stringify(monograph);
	console.log(monographJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/monographs';
	Http.open("PUT", url);
	Http.setRequestHeader("token", myActiveToken);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(monographJson);
	
	alert('Udało Ci się edytować książkę!:)'); 
	
}


function addUser(){
	var user = new Object();
	user.userName = document.getElementsByName("user_name")[0].value;
	user.email = document.getElementsByName("user_email")[0].value;
	user.password = document.getElementsByName("user_pass")[0].value;
	var userJson = JSON.stringify(user);
	console.log(userJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/users';
	Http.open("PUT", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(userJson);
	
	alert('Udało Ci się zarejestrować. Witaj!:)');
	
}

function logInUser(){
	var login = new Object();
	login.userName = document.getElementsByName("log")[0].value;
	login.password = document.getElementsByName("pass")[0].value;
	var loginJson = JSON.stringify(login);
	console.log(loginJson);
	
	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/login2';
	Http.open("PUT", url);
	Http.setRequestHeader("Content-type", "application/json");
	Http.send(loginJson);
	
	Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)
	
	
	if(Http.responseText === 'NO_USER'){
		alert("Nie ma takiego użytkownika!");
	}
	else
		{
		if(Http.responseText === 'WRONG_PASSWORD!')
		{
			alert("Błąd hasła");
		}
		else
		{
			myActiveToken = Http.responseText;
			alert("Udało Ci się zalogować!");
			// jeśli użytkownik jest zalogowany to zrób to co poniżej:	/DODAJ WARUNEK	
			document.getElementById("logowanie").style.display = "none";
			document.getElementById("moja_aplikacja").style.display = "block";
		}
	}
		
	}
	}
	
}

function logMeOut(){
//	wylogowuje uzytkownika
//	przekierowuje go na 'interfejs' logowania

	const Http = new XMLHttpRequest();
	const url='http://localhost:8080/logout';
	Http.open("PUT", url);
// odbiera token	
	Http.setRequestHeader("token", myActiveToken);
// wysyla go do backendu	
	Http.setRequestHeader("Content-type", "application/json");
	Http.send();

	
	Http.onreadystatechange=(e)=>{
    if (Http.readyState == 4 && Http.status == 200) {
	console.log(Http.responseText)

	if(Http.responseText === 'NO_SESSION'){
		alert("Udało Ci się wylogować!");
	}
	document.getElementById("logowanie").style.display = "block";
	document.getElementById("moja_aplikacja").style.display = "none";
	}
}
}

