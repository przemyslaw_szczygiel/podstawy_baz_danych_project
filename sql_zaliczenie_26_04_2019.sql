CREATE TABLE users (
	id_user INT,
	login VARCHAR(30) NOT NULL,
	email VARCHAR(40) NOT NULL,
	password VARCHAR(30) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY(id_user)
);

CREATE TABLE authors (
	id_author INT,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
    CONSTRAINT author_pk PRIMARY KEY(id_author)
);

CREATE TABLE monographs (
	id_mono INT,
	title VARCHAR(150) NOT NULL,
	publishing_house VARCHAR(50),
	city VARCHAR(30),
	year INT,
    CONSTRAINT mono_pk PRIMARY KEY(id_mono)
);

CREATE TABLE mono_authorship (
	id_mono INT,
	id_author INT,
	id_mono_authorship INT,
	CONSTRAINT mono_authorship_pk PRIMARY KEY(id_mono_authorship),
	CONSTRAINT monographs_fk_1 FOREIGN KEY (id_mono)
	REFERENCES monographs (id_mono),
	CONSTRAINT authors_fk_2 FOREIGN KEY (id_author)
	REFERENCES authors (id_author)
);

INSERT INTO authors (author_id, first_name, last_name) VALUES (1, 'Knud', 'Illeris');
INSERT INTO authors (author_id, first_name, last_name) VALUES (2, 'Joanna', 'Ostrowska');

INSERT INTO monographs (author_id, title, publishing_house, city, year) VALUES
(1, 'Trzy wymiary uczenia sie',
'DSW',
'Wroc�aw',
2006);

INSERT INTO monographs (author_id, title, publishing_house, city, year) VALUES
(2, 'Przemilczane. Seksualna praca przymusowa w czasie II wojny swiatowej',
'Marginesy',
'Warszawa',
2018);

commit;
