CREATE TABLE users (
	id_user INT,
	login VARCHAR(30) NOT NULL,
	email VARCHAR(40) NOT NULL,
	password VARCHAR(30) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY(id_user)
);

CREATE TABLE login (
	id_login INT,
	login VARCHAR(30) NOT NULL,
	password VARCHAR(30) NOT NULL,
    CONSTRAINT login_pk PRIMARY KEY(id_login)
);

CREATE TABLE authors (
	id_author INT,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
    CONSTRAINT author_pk PRIMARY KEY(id_author)
);

CREATE TABLE monographs (
	id_mono INT,
	title VARCHAR(50) NOT NULL,
	publishing_house VARCHAR(50),
	city VARCHAR(30),
	year INT,
    	CONSTRAINT mono_pk PRIMARY KEY(id_mono),
	CONSTRAINT authors_fk_2 FOREIGN KEY (id_author)
	REFERENCES authors (id_author)
);

commit;



