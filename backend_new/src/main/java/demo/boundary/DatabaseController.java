package demo.boundary;

import demo.control.AuthorsRepository;
import demo.control.MonographsRepository;
import demo.control.UsersRepository;
import demo.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@CrossOrigin
@RestController
public class DatabaseController {
// autoryzacja -> mozliwosc edytowania strony tylko kiedy token jest aktywny

    private void tokenAuthorisation(String myActiveToken) {
        boolean exists = UserController.activeTokens.contains(myActiveToken);
        if (exists == true) {
            System.out.println("Istnieje taki token:)");
        } else {
            System.out.println("Nie ma takiego tokenu");
            throw new RuntimeException();

        }
    }

    @Autowired
    private AuthorsRepository authorsRepository;

    @Autowired
    private MonographsRepository monographsRepository;

    @Autowired
    private UsersRepository usersRepository;

    @PutMapping(path = "/authors", consumes = "application/json")
    public void putAuthor(@RequestBody Author author, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        authorsRepository.save(author);
    }

    @RequestMapping(path = "/authors")
    public List<Author> getAllAuthors(@RequestHeader("token") String myActiveToken)
    {

        tokenAuthorisation(myActiveToken);

        Iterable<Author> authors = authorsRepository.findAll();
        List<Author> authorList = new ArrayList<>();
        Iterator<Author> iterator = authors.iterator();
        while(iterator.hasNext()) {
            authorList.add(iterator.next());
        }
        return authorList;
    }


    @PutMapping(path = "/monographs", consumes = "application/json")
    public void putMonograph(@RequestBody Monograph monograph, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);
        monographsRepository.save(monograph);
    }

    @RequestMapping(path = "/monographs")
    public List<Monograph> getAllMonographs(@RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken);

        Iterable<Monograph> monographs = monographsRepository.findAll();
        List<Monograph> monographList = new ArrayList<>();
        Iterator<Monograph> iterator = monographs.iterator();
        while(iterator.hasNext()) {
            monographList.add(iterator.next());
        }
        return monographList;
    }

    @DeleteMapping(path = "/monographs/{id}")
    public void deleteMonograph(@PathVariable int id, @RequestHeader("token") String myActiveToken)
    {
        tokenAuthorisation(myActiveToken); monographsRepository.deleteById(id);
    }

}
