package demo.boundary;

import demo.control.UsersRepository;
import demo.data.Author;
import demo.data.Login;
import demo.data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
public class UserController {
//    @Autowired

    public static List<String> activeTokens = new ArrayList<>();

    @Autowired
    private UsersRepository usersRepository;

// zapisywanie nowego użytkownika
    @PutMapping(path = "/users", consumes = "application/json")
    public void putUser(@RequestBody User user) {
        User userFirst = usersRepository.findByUserName(user.getUserName());
        if (userFirst == null) {
            System.out.println("Brawo! Nie ma jeszcze użytkownika o podanej nazwie!");

// fragment kodu ktory dokonuje enkrypcji hasła podanego przez uzytkownika podczas rejestracji
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(user.getPassword().getBytes());
                byte[] digest = md5.digest();
                String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
                user.setPassword(myHash);
            }
            catch (NoSuchAlgorithmException e){
                System.out.println("no algoritm");
            }
// zapisywanie uzytkownika do bazy danych
            usersRepository.save(user);
        } else {
            System.out.println("Użytkownik o podanej nazwie już istnieje");
        }
    }

// odczytywanie listy użytkowników
    @RequestMapping(path = "/users")
    public List<User> getAllUsers() {

        Iterable<User> users = usersRepository.findAll();
        List<User> userList = new ArrayList<>();
        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) {
            userList.add(iterator.next());
        }
        return userList;
    }

// logowanie użytkownika + odpowiednie komunikaty do backendu
    @PutMapping(path = "/login2", consumes = "application/json")
    public String putUser(@RequestBody Login login) {
        User user = usersRepository.findByUserName(login.getUserName());

        if (user == null) {
            System.out.println("Nie istnieje taki użytkownik.");
            return "NO_USER";
        } else {
        String myHash = null;
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(login.getPassword().getBytes());
                byte[] digest = md5.digest();
                myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
            }
            catch (NoSuchAlgorithmException e){
                System.out.println("no algoritm");
            }

            if (user.getPassword().equals(myHash)) {
                System.out.println("Użytkownik o podanej nazwie istnieje.");
                String uuid = UUID.randomUUID().toString();
                activeTokens.add(uuid);
                return uuid;
            } else {
                System.out.println("Błąd hasła!");
                return "WRONG_PASSWORD!";
            }
        }
    }


    @PutMapping(path = "/logout", consumes = "application/json")
    public String logoutUser(@RequestHeader("token") String myActiveToken) {
        activeTokens.remove(myActiveToken);
        System.out.println("Użytkownik został poprawnie wylogowany.");
        return "NO_SESSION";
    }
}

