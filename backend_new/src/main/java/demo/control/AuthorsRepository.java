package demo.control;

import demo.data.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Table;

//@Repository
public interface AuthorsRepository extends CrudRepository<Author, Integer> {

}