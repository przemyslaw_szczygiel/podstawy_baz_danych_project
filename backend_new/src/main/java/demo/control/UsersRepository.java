package demo.control;

import demo.data.User;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<User, Integer> {
    User findByUserName(String userName);
}
