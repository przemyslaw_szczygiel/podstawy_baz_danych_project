package demo.control;

import demo.data.Monograph;
import org.springframework.data.repository.CrudRepository;

public interface MonographsRepository extends CrudRepository<Monograph, Integer> {

}
