package demo.data;

import javax.persistence.*;

@Table(name = "MONOGRAPHS")
@Entity // This tells Hibernate to make a table out of this class
public class Monograph {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int monographId;
    private String title;
    private String publishingHouse;
    private String city;
    private int year;

    @OneToOne
    @JoinColumn(name = "authorId")
    private Author author;

    public int getMonographId() {
        return monographId;
    }

    public void setMonographId(int monographId) {
        this.monographId = monographId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
