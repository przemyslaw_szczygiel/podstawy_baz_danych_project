package demo.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;

@Table(name = "AUTHORS")
@Entity // This tells Hibernate to make a table out of this class
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int authorId;
    private String firstName;
    private String lastName;

    public Author() {

    }

    public Author(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Author author = mapper.reader().forType(Author.class).readValue(json);
            this.authorId = author.authorId;
            this.firstName = author.firstName;
            this.lastName = author.lastName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Author(int authorId, String firstName, String lastName) {
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}